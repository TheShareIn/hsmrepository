package com.amazonaws.cloudhsm.examples;

import com.cavium.cfm2.ImportKey;
import com.cavium.cfm2.Util;
import com.cavium.key.CaviumKey;
import com.cavium.key.parameter.CaviumKeyGenAlgorithmParameterSpec;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.io.Console;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.InvalidParameterException;
import java.security.Key;
import java.security.Security;
import java.util.Enumeration;

public class KeyGenerateAlbo {


    public static byte[] generate(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    public static byte[] buildKey(byte[] cc1, byte[] cc2) {
        byte[] result = new byte[cc1.length];
        int i = 0;
        for (byte b1: cc1) {
            byte b2 = cc2[i];
            result[i] = (byte)(b1 ^ b2 );
            i++;
        }
        return result;
    }
    public static byte[] completeArray(byte[] bytes) {
        byte[] result = new byte[32];
        System.arraycopy(bytes, 0, result, 32 - 16, 16);
        return result;
    }
    public static String bytesToHex(byte[] bytes) {
        final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }


    private static Key importKey(Key key, String keyLabel, boolean isExtractable, boolean isPersistent) {
        // Create a new key parameter spec to identify the key. Specify a label
        // and Boolean values for extractable and persistent.
        CaviumKeyGenAlgorithmParameterSpec spec = new CaviumKeyGenAlgorithmParameterSpec(keyLabel, isExtractable, isPersistent);
        try {
            Key importedKey = ImportKey.importKey(key, spec);
            return importedKey;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static void displayKeyInfo(CaviumKey key) {
        if (null != key) {
            System.out.printf("Key handle %d with label %s\n", key.getHandle(), key.getLabel());
            // Display whether the key can be extracted from the HSM.
            System.out.println("Is Key Extractable? : " + key.isExtractable());

            // Display whether this key is a token key.
            System.out.println("Is Key Persistent? : " + key.isPersistent());

            // The algorithm and size used to generate this key.
            System.out.println("Key Algo : " + key.getAlgorithm());
            System.out.println("Key Size : " + key.getSize());
        }
    }

    public static void main(String[] args) throws Exception {

        String component1 = "447FC2AA6EFFFEE5405A559E88DC958C";
        String component2 = "1086F0493DB0EFE42EDF1BC99541E96F";
        String partition = "PARTITION_1";
        String user = "erick";
        String password = "albo2021";
        String label = "keyTest";

        System.setProperty("HSM_PARTITION", partition);
        System.setProperty("HSM_USER", user);
        System.setProperty("HSM_PASSWORD", password);

        try {
            Security.addProvider(new com.cavium.provider.CaviumProvider());
        } catch (IOException ex) {
            System.out.println(ex);
            return;
        }

        try {
            byte[] cc1 = generate(component1);
            byte[] cc2 = generate(component2);
            byte[] ccparcial = buildKey(cc1,cc2);
            String plainKey = bytesToHex(ccparcial);

            String algorithm = "DESede";
            if(plainKey.length() != 32 && plainKey.length() != 48) throw new InvalidParameterException("the key argument needs to be either 32 or 48 characters");
            String fullKey = plainKey;
            if(plainKey.length() == 32) fullKey = plainKey + plainKey.substring(0, 16);
            byte[] keyValue = Hex.decodeHex(fullKey.toCharArray());
            DESedeKeySpec keySpec = new DESedeKeySpec(keyValue);
            SecretKey secKey = SecretKeyFactory.getInstance(algorithm).generateSecret(keySpec);
            Key importedKey = importKey(secKey, label, false, true);
            System.out.println("================****************==============");
            displayKeyInfo((CaviumKey) importedKey);
            System.out.println("================****************==============");
        }catch (Exception e){
            System.out.println("ERROR -> Error to save the key");
        }



        try {
            String formatStringForKeyDetails = "%-12s%-12s%-12s%-12s%-12s%s\n";

            System.out.format(formatStringForKeyDetails, "KeyHandle", "Persistent",
                    "Extractable", "Algo", "Size", "Label");
            for(Enumeration<CaviumKey> keys = Util.findAllKeys(null); keys.hasMoreElements();) {
                CaviumKey k = keys.nextElement();
                System.out.format(formatStringForKeyDetails, k.getHandle(), k.isPersistent(),
                        k.isExtractable(), k.getAlgorithm(), k.getSize(), k.getLabel());
            }

        }catch (Exception e){
            System.out.println("ERROR -> Error to recover keys");

        }


    }
}
